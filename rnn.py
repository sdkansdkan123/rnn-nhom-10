import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation
from keras.optimizers import RMSprop, Adam
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

SEQ_LENGTH = 100


def buildmodel(VOCABULARY):
    model = Sequential()
    model.add(LSTM(256, input_shape=(SEQ_LENGTH, 1), return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(256))
    model.add(Dropout(0.2))
    model.add(Dense(VOCABULARY, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam')
    return model


file = open('full.txt', encoding='utf8')
raw_text = file.read()
raw_text = raw_text.lower()

chars = sorted(list(set(raw_text)))
print(chars)
# bad_chars = ['#', '*', '@', '_','\n','\\','//','`','-', '/','–', '\x02', '(', ')', "'", '”','“' ,'\ufeff',
# '‘', '’', '…', '⁄', '东', '买', '今', '他', '们', '天', '的', '西','!', '+', ':', ';', '>', '?', '[', ']','‘', '’', '…']
#
bad_chars = ['#', 'å', '*', '@', '_', '\n', '\\', '//', '`', '-', '/', '–', '\x02', '(', ')', "'", '”', '“', '\ufeff',
             '‘', '’', '…', '⁄', '东', '买', '今', '他', '们', '天', '的', '西', '!', '+', ':', ';', '>', '?',
             '\t', '%', '[', ']', '‘', '’', '…', '̀', '́', '̂', '̃', '̆', '̉', '̛', '̣', 'δ',
             '•', '→', '、', '。', 'い', 'く', 'さ', 'し', 'た', 'っ', 'て', 'に', 'は', 'ま', 'を', 'ん', '族', '日', 'º', 'ð',
             '昨', '水', '私', '美', '行', '見', '館', '魚', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

for i in range(len(bad_chars)):
    raw_text = raw_text.replace(bad_chars[i], "")
chars = sorted(list(set(raw_text)))
print(chars)
VOCABULARY = len(chars)

int_to_char = dict((i, c) for i, c in enumerate(chars))
char_to_int = dict((c, i) for i, c in enumerate(chars))

filename = 'hh.hdf5'
model = buildmodel(VOCABULARY)
model.load_weights(filename)
model.compile(loss='categorical_crossentropy', optimizer='adam')

original_text = ''
predicted_text = []


def cls():
    print("\n" * 50)


while True:
    # print(original_text)
    original_text = original_text.replace("  ", " ")  # fix 2 dau cach
    print('1')
    text = input(original_text + " ")
    print('2')
    #
    #
    #
    # cls()
    print("\n\n")
    text = text.lower()  ## khoi vang loi uppercase

    if text == '`':  # ~`  > use predict nhu new input
        text = predicted_text[len(predicted_text) - 1].replace(" ", "")
    if text == '//':
        break
    inp = list(original_text + ' ' + text)
    inp.pop(0)
    #    print('------------\n predicted_text: ',predicted_text)
    #    print('original_text: ', original_text)
    #   print('inp: ', inp) ##

    last_word = inp[len(original_text):]
    inp = inp[:len(original_text)]
    original_text = original_text + ' ' + text
    last_word.append(' ')
    #    print('inp ',inp)
    #    print('last_word ',last_word)
    #    print('original_text: ', original_text)
    ########
    inp_text = [char_to_int[c] for c in inp]

    last_word = [char_to_int[c] for c in last_word]

    #   print('inp_text ',inp_text)
    #    print('last_word ', last_word)

    if (len(inp_text) > 100):
        inp_text = inp_text[len(inp_text) - 100:]
    if len(inp_text) < 100:
        pad = []
        space = char_to_int[' ']
        pad = [space for i in range(100 - len(inp_text))]
        inp_text = pad + inp_text

    while len(last_word) > 0:
        X = np.reshape(inp_text, (1, SEQ_LENGTH, 1))
        next_char = model.predict(X / float(VOCABULARY))
        inp_text.append(last_word[0])
        inp_text = inp_text[1:]
        last_word.pop(0)
    # print(int_to_char[np.argmax(next_char)])
    #   print('inp_text ',inp_text)
    #  print('last_word ', last_word)
    next_word = []
    next_char = ':'
    while next_char != ' ':
        X = np.reshape(inp_text, (1, SEQ_LENGTH, 1))
        next_char = model.predict(X / float(VOCABULARY))
        index = np.argmax(next_char)
        next_word.append(int_to_char[index])
        inp_text.append(index)
        inp_text = inp_text[1:]
        next_char = int_to_char[index]

    predicted_text = predicted_text + [''.join(next_word)]
    print("(Du doan: " + ''.join(next_word), end=')')

from tabulate import tabulate

original_text = original_text.split()
predicted_text.insert(0, "")
predicted_text.pop()

table = []
dem = 0
for i in range(len(original_text)):
    if (original_text[i].replace(" ", "") == predicted_text[i].replace(" ", "")):
        dem = dem + 1
        table.append([original_text[i], predicted_text[i], str(dem)])
    else:
        table.append([original_text[i], predicted_text[i], 'flase'])
print(tabulate(table, headers=['Actual Word', 'Predicted Word', 'Resutf']))

